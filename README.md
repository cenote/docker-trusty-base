# trusty-base

Based on ubuntu:14.04

This Docker image is the base image for the java build images.
It contains any software that the other images have in common.

Installed software:

* ca-certificates
* curl
* fontconfig (making ttf-mscorefonts available to java)
* git
* lib32ncurses5 (needed for launch4j-maven-plugin)
* lib32z1 (needed for launch4j-maven-plugin)
* libstdc++6:i386 (needed for nsis)
* nsis-2.46
* openssh-client
* software-properties-common
* ttf-mscorefonts-installer
* wget
